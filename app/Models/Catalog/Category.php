<?php
namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class Category extends BaseModel {

    public function child() {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public function parent() {
        return $this->hasOne(Category::class, 'id', 'parent_id');
    }

    public function link() {

    }

}
