<?php
namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class Product extends BaseModel {

    public function categories() {
        return $this->belongsToMany(Category::class, 'product_categories', 'product_id', 'category_id');
    }

    public function link() {

    }
}
